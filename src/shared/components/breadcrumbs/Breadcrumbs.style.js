import styled from 'styled-components';

export const BreadcrumbsContainer = styled.ol`
  display: flex;
  padding: 0.75rem 1rem;
  align-items: center;
  justify-content: center;   
`;

export const BreadcrumbsDivider = styled.span`
  padding: 0 5 px;
`;

export const BreadcrumbItem = styled.li`
  margin-bottom: 1rem;
  list-style: none;
  a {
    color: #000000;
    &.active {
      color: #7D7D7D;
      text-decoration: none;
      cursor: pointer;
    }
    &:after {
      display: inline-block;
      padding 0 0.5rem;
      color: #6c757d;
      content: ">";
    }
    &:last-child {
      &:after {
      display: none;
      }
    }
  }
`;

export const BreadcrumbLink = styled.a`
  color: #000000;
  &.active {
    color: #7D7D7D;
    text-decoration: none;
    cursor: initial;
    }
  }  
`;
