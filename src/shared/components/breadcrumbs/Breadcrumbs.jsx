import React from 'react';
import {BreadcrumbLink, BreadcrumbsContainer, BreadcrumbsDivider} from "./Breadcrumbs.style";
import {matchPath} from 'react-router-dom';
import withRouter from "react-router-dom/es/withRouter";

const renderer = ({breadcrumb, match}) => {
    if (typeof breadcrumb === 'function') {
        return breadcrumb({match});
    }
    return breadcrumb;
};

const getBreadcrumbs = ({routes, pathname}) => {
    const matches =[];

    pathname
        .replace(/\/$/, '')
        .split('/')
        .reduce((previous, current) => {
            const pathSection = `${previous}/${current}`;

            let breadcrumbMatch;

            routes.some(({breadcrumb, path}) => {
                const match = matchPath(pathSection, {exact: true, path});

                if (match) {
                    breadcrumbMatch = {
                        breadcrumb: renderer({breadcrumb, match}),
                        path,
                        match,
                    };
                    return true;
                }

                return false;
            });

            if (breadcrumbMatch) {
                matches.push(breadcrumbMatch);
            }

            return pathSection;
        });

    return matches;
};

const withBreadcrumbs = (menuItems) => (Component) =>
    withRouter((menuItems) => (
        <Component {...menuItems}
            breadcrumb={getBreadcrumbs ({
                    pathname: menuItems.location.pathname,
                    routes,
            })}
        />
    ));

// const FirstLevelBreadcrumb = ({match}) => <span>{match.params.firstLevel}</span>;
// const SecondLevelBreadcrumb = ({ match }) => <span>{match.params.secondLevel}</span>;

const routes = [
    {
        path:'/Home', breadcrumb: 'Home'
    },
    {
        path: '/digital-tech', breadcrumb: 'Цифровая техника'
    },
    // {
    //     path: '/test2/:firstLevel', breadcrumb: FirstLevelBreadcrumb
    // },
    // {
    //     path: '/test2/:firstLevel/:secondLevel', breadcrumb: SecondLevelBreadcrumb
    // },
    {
        path: '/optics', breadcrumb: 'Oптика для фото'
    },
];

let Breadcrumbs = ({breadcrumbs}) => {
    return (
        <BreadcrumbsContainer>
            {breadcrumbs.map(({ breadcrumb, path, match }, i) => (
                <span key={path}>
					<BreadcrumbLink exact={true} to={match.url}>
						{breadcrumb}
					</BreadcrumbLink>
                    {i !== breadcrumbs.length - 1 && <BreadcrumbsDivider> > </BreadcrumbsDivider>}
				</span>
            ))}
        </BreadcrumbsContainer>
    );
};

Breadcrumbs = withBreadcrumbs(routes)(Breadcrumbs);

export default Breadcrumbs;

