import styled from 'styled-components';
import {ReactComponent as LogoImage} from "./LogoImage.svg";

export const LogoStyled = styled(LogoImage)`
  margin: 0 2px;
  fill: {(props) => {props.isColored ? #DF2725 : #B7B7B7)};
`;

export const TextFront = styled.h1`
  display: inline-block;
  font-size: 42px;
  color: {(props) => (props.isColored ? #DF2725 : #B7B7B7)};
`;

export const TextBack = styled.h1`
  display: inline-block;
  font-size: 42px;
  font-weight: bold;
  color: {(props) => (props.isColored ? #51AD33 : #B7B7B7)};
`;
