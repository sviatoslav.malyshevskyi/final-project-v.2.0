import styled from 'styled-components';

export const Input = styled.input`
  width: 100%;
  margin-bottom: 5px;
  padding: 15px;
  color: #677283;
  border: 1px solid #B7B7B7;
  border-radius: 5px;
  box-sizing: border-box;
  
  &::placeholder {
    color: #B7B7B7;
  }
`;

export const Dropdown = styled.select`
  ${Input};
`;
