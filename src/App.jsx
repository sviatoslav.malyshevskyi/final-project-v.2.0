import React from 'react';
import {GlobalStyle, MainWrapper} from "./shared/styles/global.style";
import Header from "./client/components/header/Header";
import AppRoutes from "./app/routes";
import ROUTES from "./app/app.router";
import {BrowserRouter} from "react-router-dom";
import Breadcrumbs from "./shared/components/breadcrumbs/Breadcrumbs";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <GlobalStyle />
        <MainWrapper>
          <Header />
          <Breadcrumbs />
          <AppRoutes routes={ROUTES} />
        </MainWrapper>
      </BrowserRouter>

    </div>
  );
}

export default App;
