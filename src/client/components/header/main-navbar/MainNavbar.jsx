import React from 'react';
import {menuItems} from "./menuItems.json";
import {MainMenuItem, MainMenuList, MainNavContainer} from "./MainNavbar.style";

const MainNavbar = () => {
  const menuItemsElements = menuItems.map(menuItems =>
      <MainMenuItem exact to={menuItems.href} key={menuItems.id}>{menuItems.title}</MainMenuItem>
  )
  return (
      <MainNavContainer>
        <MainMenuList>
          {menuItemsElements}
        </MainMenuList>
      </MainNavContainer>
  );
};

export default MainNavbar;
