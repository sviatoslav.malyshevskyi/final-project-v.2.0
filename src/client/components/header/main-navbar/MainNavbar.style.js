import styled from 'styled-components';
import {FullWidthContainer} from "../../../../shared/styles/global.style";

export const MainNavContainer = styled(FullWidthContainer)`
  max-width: 1400px;
  background-color: #51AD33;
`;

export const MainMenuList = styled.ul`
  display: flex;
  height: 50px;
  margin-left: 164px;
  align-items: center;
  justify-content: start;
`;

export const MainMenuItem = styled.li`
  padding-right: 30px;
  font-family: Roboto;
  font-size: 14px;
  font-weight: 400;
  line-height: 27px;
  color: #ffffff;
  cursor: pointer;
`;
