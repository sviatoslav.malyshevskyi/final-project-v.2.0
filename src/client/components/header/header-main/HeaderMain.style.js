import styled from 'styled-components';
import {ReactComponent as SearchLogo} from "./SearchLogo.svg";
import {FullWidthContainer} from "../../../../shared/styles/global.style";
import {Dropdown} from "../../../../shared/styles/forms.style";

export const HeaderMainContainer = styled(FullWidthContainer)`
  display: flex;
  max-width: 1200px;
  height: 90px;
  align-items: center;
  justify-content: start;
`;

export const phoneNumbers = styled(Dropdown)`
  font-family: Roboto;
  font-weight: 900;
  line-height: 18.7;
  text-align: center;
`;

export const HeaderIconsContainer = styled.div`
  display: flex;
  width: 250px;
  margin-left: 80px;
  align-items: center;
  justify-content: start;
`;

export const SearchIcon = styled(SearchLogo)`
  width: 32px;
  height: 32px;
  margin-right: 50px;
  fill: #ffffff;
`;

export const SearchInput = styled.input`
  border: none;
 
`;
