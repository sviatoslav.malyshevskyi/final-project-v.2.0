import React from 'react';
import {Link} from '../../../../../shared/styles/global.style';
import {CartText, CartWidgetBadge, CartImage, CartWidgetContainer} from "./CartWidget.style";

const CartWidget = () => {
  return (
      <Link to="/cart">
        <CartWidgetContainer>
          <CartImage />
          <CartWidgetBadge>4</CartWidgetBadge>
          <CartText>Корзина</CartText>
        </CartWidgetContainer>
      </Link>
  );
};

export default CartWidget;
