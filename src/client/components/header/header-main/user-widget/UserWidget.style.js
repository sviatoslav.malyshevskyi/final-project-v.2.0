import styled from 'styled-components';
import {ReactComponent as UserLogo} from "./UserImage.svg";

export const UserWidgetContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const UserImage = styled(UserLogo)`
  width: 41px;
  height: 41px;
  stroke: #ffffff;
  fill: {(props) => props.isLoggedIn ? #51AD33 : #B7B7B7};
`;

export const UserName = styled.h2`
  margin-left: 9px;
  margin-right: 90px;
  font-size: 16px;
`;
