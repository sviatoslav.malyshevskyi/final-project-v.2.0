import React from 'react';
import {UserWidgetContainer, UserImage, UserName} from "./UserWidget.style";

const UserWidget = () => {

  return (
      <UserWidgetContainer>
        <UserImage />
        <UserName>Вход</UserName>
      </UserWidgetContainer>
  );
}

export default UserWidget;
