import React from 'react';
import Logo from "../../../../shared/components/logo/Logo";
import {HeaderIconsContainer, HeaderMainContainer, SearchIcon, SearchInput} from "./HeaderMain.style";
import CartWidget from "./cart-widget/CartWidget";
import UserWidget from "./user-widget/UserWidget";

const HeaderMain = () => {
  return (
      <HeaderMainContainer>
        <Logo isColored />
        <SearchInput
            type="text"
            name="query"
        />
        <SearchIcon />

        <phoneNumbers>
          0(800) 21 21 50 &#x2304;
        </phoneNumbers>
        <HeaderIconsContainer>
          <UserWidget />
          <CartWidget />
        </HeaderIconsContainer>
      </HeaderMainContainer>
  );
}

export default HeaderMain;
