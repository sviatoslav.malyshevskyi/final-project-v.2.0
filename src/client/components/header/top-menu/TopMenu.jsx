import React from 'react';
import {topMenuData} from './topMenuData.json';
import {TopMenuItem, TopMenuList} from "./TopMenu.style";
import {FullWidthContainer} from "../../../../shared/styles/global.style";

const TopMenu = () => {
  const TopMenuItemsElements = topMenuData.map(topMenuData =>
      <TopMenuItem to={topMenuData.href} key={topMenuData.id}>{topMenuData.title}</TopMenuItem>
  )

  return (
      <FullWidthContainer>
        <TopMenuList>
          {TopMenuItemsElements}
        </TopMenuList>
      </FullWidthContainer>
  );
};

export default TopMenu;
