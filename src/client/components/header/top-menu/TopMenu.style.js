import styled from 'styled-components';

export const TopMenuList = styled.ul`
  display: flex;
  height: 35px;
  align-items: center;
  justify-content: start;
`;

export const TopMenuItem = styled.li`
  padding-right: 34px;
  font-family: Roboto, sans-serif;
  font-size: 12px;
  font-weight: 300;
  line-height: 14px;
  text-transform: capitalize;
  cursor: pointer;
`;
