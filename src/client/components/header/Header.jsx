import React from 'react';
import TopMenu from "./top-menu/TopMenu";
import HeaderMain from "./header-main/HeaderMain";
import MainNavbar from "./main-navbar/MainNavbar";

const Header = () => {
  return (
      <header>
        <TopMenu />
        <HeaderMain />
        <MainNavbar />
      </header>
  );
}

export default Header;
