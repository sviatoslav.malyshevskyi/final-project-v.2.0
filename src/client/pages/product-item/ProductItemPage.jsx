import React from 'react';
import {MainWrapper} from "../../../shared/styles/global.style";

const ProductItemPage = () => {

  return (
      <MainWrapper>
        <p>Product Item Page</p>
      </MainWrapper>
  );
};

export default ProductItemPage;
