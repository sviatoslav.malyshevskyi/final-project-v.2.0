import React from 'react';
import {MainWrapper} from "../../../shared/styles/global.style";

const CartPage = () => {

  return (
      <MainWrapper>
        <p>Shopping Cart Page</p>
      </MainWrapper>
  );
}

export default CartPage;
