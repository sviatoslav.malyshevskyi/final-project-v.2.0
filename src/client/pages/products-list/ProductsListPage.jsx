import React from 'react';
import {MainWrapper} from "../../../shared/styles/global.style";

const ProductsListPage = () => {

  return (
      <MainWrapper>
        <p>Products List Page</p>
      </MainWrapper>
  );
}

export default ProductsListPage;
