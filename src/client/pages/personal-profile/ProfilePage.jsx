import React from "react";
import {MainWrapper} from "../../../shared/styles/global.style";

const ProfilePage = () => {
  return (
      <MainWrapper>
        <p>Personal Profile Page</p>
      </MainWrapper>
  );
}

export default ProfilePage;
