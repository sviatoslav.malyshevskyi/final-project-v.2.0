import React from 'react';
import {MainWrapper} from "../../../shared/styles/global.style";

const HomePage = () => {
  return (
      <MainWrapper>
        <p>Home Page</p>
      </MainWrapper>
  );
}

export default HomePage;
