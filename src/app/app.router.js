import HomePage from "../client/pages/home/HomePage";
import CheckoutPage from "../client/pages/checkout";
import ProfilePage from "../client/pages/personal-profile";
import ProductItemPage from "../client/pages/product-item";
import ProductsListPage from "../client/pages/products-list";
import CartPage from "../client/pages/shopping-cart";

const ROUTES = [
  {
    exact: true,
    path: '/',
    component: HomePage,
  },
  {
    exact: true,
    path: '/checkout',
    component: CheckoutPage,
  },
  {
    exact: true,
    path: '/profile',
    component: ProfilePage,
  },
  {
    exact: true,
    path: '/product',
    component: ProductItemPage,
  },
  {
    exact: true,
    path: '/products-list',
    component: ProductsListPage,
  },
  {
    exact: true,
    path: '/cart',
    component: CartPage,
  }
];

export default ROUTES;
