import React from 'react';
import {Switch, Route} from 'react-router-dom';
import HomePage from "../../client/pages/home";

const AppRoutes = () => {
  return (
    <Switch>
      <Route exact path='/'>
        <HomePage />
      </Route>
    </Switch>
  );
}

export default AppRoutes;
